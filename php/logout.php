<?php
include 'db.php';
include 'common.php';
session_start();
$_SESSION["maskitbe_user_id"] = "";
$_SESSION["maskitbe_logged_in"] = false;
unset($_SESSION["maskitbe_user_id"]);
unset($_SESSION["maskitbe_logged_in"]);
session_destroy();
